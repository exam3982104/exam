const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors('*'))
app.use(express.json())

const router = require('./Router/movie')
app.use('/movie',router)



app.listen(5000,'0.0.0.0',()=>{

    console.log("Server Started on port 3300 Thanks")
})