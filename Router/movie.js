const express = require('express')
const router = express.Router()
const db = require('../db')
const utils = require('../utils')
router.get("/",(req,res)=>{
    const query = 'select * from movie_tb'
    db.pool.execute(query,(error,data)=>{
        res.send(utils.CreateResult(error,data))
    })
})
router.post("/",(req,res)=>{
    const { movie_title,movie_date,movie_time,director_name }=req.body
    const query = 'insert into movie_tb(movie_title,movie_date,movie_time,director_name) values (?,?,?,?)'
    db.pool.execute(query,[movie_title,movie_date,movie_time,director_name],(error,data)=>{
        res.send(utils.CreateResult(error,data))
    })
})

router.delete("/:id",(req,res)=>{
    const {id}=req.params;
    const query = 'delete from movie_tb where movie_id = ?'
    db.pool.execute(query,[id],(error,data)=>{
        res.send(utils.CreateResult(error,data))
    })
})









module.exports=router